---
output:
  pdf_document: default
  html_document: default
---

# Author's Response

> Is there a reason the "Paper preparation and review code" couldn't also be
> linked in the manuscript itself? Some of that might be useful to readers, like
> the mapping .csv files. Or is everything like that in the datasets that you've
> already linked?

Those mappings are in the process of being moved into the linked R packages, but
they are not included in the package versions the paper is based on. I included
the Zenodo reference to the material in the paper (line 482), but it seems that
`latexdiff` does not work inside the `\codeavailability{}` environment, so it
was not properly highlighted in the mark-up pdf.


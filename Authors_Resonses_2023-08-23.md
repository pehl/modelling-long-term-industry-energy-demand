---
output:
  pdf_document: default
  html_document: default
---

# Author's Response

> Looks like a very interesting paper. I've changed its type to "Model
> description paper," which I think it fits with better:
> <https://www.geoscientific-model-development.net/about/manuscript_types.html>

Thank you for evaluating our paper and suggesting improvements! I assumed
"Development and technical paper" is a better fit since we are improving a model
that has been described in a "Model description" paper before, but I do not mind
to have assumed wrongly.

> Just a couple of requests before I send it to reviewers: - If it is possible
> to make the non-proprietary parts of the input data and paper preparation code
> available while still restricting access to the proprietary data, please do
> so.

I did so. The code for preparing the paper is available at Zenodo
(<https://zenodo.org/record/8276423>). The proprietary data source we are not
allowed to share (UNIDO) is included as a symbolic link to a directory
`UNIDO_proprietary`, which is shared with reviewers separately.

> -   Line 79: Figure reference is missing ("??").

I fixed the reference.


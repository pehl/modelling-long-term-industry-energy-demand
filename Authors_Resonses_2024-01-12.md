---
output: pdf_document
---

# Author's Response

> Thank you for adding a basic evaluation of energy demand relative to historic
> data in Appendix B. Since both reviewers brought this up, it would be useful
> to highlight, so please refer to Appendix B---and briefly summarize the
> results---somewhere in the main text.

We added a reference to Appendix B in section 4:

*For a comparison of total industry FE demand with historic data and other
projections, see Appendix B. Total FE demand of the SSP2 scenario increases in
accordance with the "Reference Technology Scenario" from International Energy
Agency (2017) until 2050 (cf. section 3.6), with SSP1 and SSP5 markedly higher
and lower, respectively.*

> -   L58: "modes" should be "models"
> -   L189: "recent" should be "forthcoming" or something similar
> -   Table 1: Extra "steel" in third row first column?
> -   L518: "Irland" typo
> -   L519: Replace "it" with "the former"
> -   Data availability statement: "propriatory" should be "proprietary"
> -   L530: "supervisedlibrar"

We addressed all these typos.

> Font is too small in Figs. 2-8

We increased the font size in the figures.

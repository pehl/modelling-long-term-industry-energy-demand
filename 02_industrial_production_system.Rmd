---
title: ''
bibliography: bibliography.bib
output: html_document
---

# The industrial production system in the macroeconomic context

## REMIND Model Structure

The integrated assessment model REMIND [@baumstark_remind21_2021] is comprised
of a macroeconomic growth model and a detailed energy system model (ESM), which
are hard-linked, i.e. optimised simultaneously, with energy supply (by the ESM)
and demand (by the macroeconomic model) quantities and prices in equilibrium.
The ESM represents over 50 conventional and low-carbon energy conversion
technologies, modelling energy flow from primary through secondary to final
energy, accounting for CO~2~ and other greenhouse gas (GHG) emissions as well as
carbon capture and sequestration (CCS), carbon capture and utilisation (CCU) and
options for carbon dioxide removal (CDR). The macroeconomy is represented by an
intertemporal general equilibrium model that maximises intertemporal welfare of
twelve to 21 world regions (depending on parametrisation) that are linked by
trade in primary energy carriers, an aggregated trade good, and emission
permits. The macroeconomic model uses a nested constant elasticity of
substitution (CES) production function [@jae_wan_chung_utility_1994], in which
labour, energy, and capital are used for production of economic output, and
energy inputs are tracked through the economic sectors buildings, industry, and
transport to final energy carriers which constitute the links to the ESM. The
different economic sectors form themselves sub-trees in the CES function and can
be realised in different levels of detail. This paper describes specifically the
extension of the industry sector from an aggregated realisation to one in which
different subsectors are modelled explicitly. The nested CES production function
takes the form

```{=tex}
\begin{equation}
\label{production_function}
V_o = \left( \sum_{(o,i) \in \mathrm{CES}} \alpha_i {V_i}^\rho \right)^{\frac{1}{\rho}}
\end{equation}
```
where $V_o$[^1] is the output quantity, $V_i$ are the input quantities,
$\alpha_i$ are efficiency parameters for the inputs, and $\rho_o$ is a parameter
derived from the substitution elasticity $\sigma_o$ ($\rho = 1 - \sigma^{-1}$)
between the inputs on a CES nest. The set $\mathrm{CES}$ of tuples $(o,i)$ links
the output of a CES nest to its inputs. Since outputs on one level are inputs on
another, this gives rise to a tree structure (cf. Figure \ref{fig:ces-tree}).
Substitution elasticities $\sigma$ in general describe the relative change in
utilisation of the inputs to an economic production process (called production
factors) in relation to the relative change in input prices. A constant
elasticity of substitution function assumes that a specific percentage increase
(decrease) in the price of one production factor in relation to the prices of
the other factors will cause a constant specific percentage decrease (increase)
in factor utilisation, irrespective of the amount of factor utilisation at which
this price change occurs.

Operating the REMIND model entails calculating a baseline scenario, in which no
climate change mitigation policies are assumed, and then imposing different
constraints on the model (e.g. a fixed carbon price trajectory, a limit on peak
and/or end-of-century temperature increase, a limited greenhouse gas emissions
budget over the century) for calculating policy scenarios that are used to
investigate specific research questions (e.g. the feasibility of climate change
mitigation targets under constrained availability of technologies, the impact of
climate change mitigation policy on investments into certain technologies, the
macroeconomic costs of different climate change mitigation regimes)
[@baumstark_remind21_2021]. Besides initialising a large set of variables (i.a.
all stocks of energy conversion technologies in the ESM, their efficiency
parameters, trade in all traded goods) for the first model time step (2005),
this requires determining the efficiency parameters $\alpha_i$ for the CES
production function for the entire model time horizon. For this, trajectories
for the output (macroeconomic production -- GDP) of the CES production function
and all inputs (final energy demand and for industry also subsector production
levels) into the CES production function over the entire model time horizon
(until 2100) are needed.

In addition to extending the industry sector in the REMIND model itself, it is
also necessary to calculate consistent trajectories for the input data of the
CES calibration (subsector production and final energy demand) for different
baseline scenarios of future development (see section
\ref{sec:general-approach}).

## Industry Subsectors

The CES production function of REMIND can be expanded to explicitly represent
the industrial subsectors and economic drivers for the demand of their products.
The industry sector of the REMIND model is split into four subsectors:
cement[^2], chemicals, steel production, and all "other industry" production,
based on the energy demand characteristics of the sectors, their portion in
industry energy demand and CO~2~ emissions, and the applicability of CCS.

The products of both the cement and steel subsectors are comparatively
homogeneous with well-defined properties, for which country-level statistics in
physical units are available, and the subsectors are dominated by few production
processes. The chemicals subsector produces a diverse range of intermediate and
final products, yet some energy intensive processes (e.g. steam cracking or
reforming) are common to many production routes [@fischedick_industry_2014].
Since production statistics for the different products are not widely available
and the products lack commensurability with respect to energy inputs and CO~2~
emissions of production, a monetary measure is employed to model the activity of
the chemicals subsector. The last subsector, "other industry", is by definition
characterised by diverse processes and heterogeneous goods which are not
comparable on a physical basis, and therefore a monetary measure is used for
modelling its activity, too. The decarbonisation challenges faced by "other
industry" production are, however, quite comparable, as most of the energy
demand can be electrified by established technologies [@madeddu_co2_2020-1].

Using value added instead of physical production to drive industry energy demand
incurs two difficulties. Both the specific value added per unit of (physical)
production and the composition of different types of products making up
subsector production vary across regions and change over time. This reduces the
interpretability of subsector production figures given in value added,
especially in absolute terms. It does, however, not impinge on the usefulness
for linking economic activity and industry energy demand, as the historical
regional differences are subsumed by the regression of subsector energy demand
on subsector activity, and the composition of subsector production is expected
to move in the direction of higher shares of high-value products (decreasing
physical production per unit value added) as economies evolve to higher GDP per
capita, which acts in the same direction of increasing energy efficiency
(decreasing energy demand per unit physical production), not introducing
behaviour that is different from subsectors with physical representation.

The production of cement, chemicals, and iron and steel consumes the bulk of
final energy in the industry sector (7 %, 14 %, and 23 %, respectively
[@fischedick_industry_2014]), and accounts for 29 %, 13 %, and 30 % of total
direct CO~2~ emissions of industry (8.7 Gt CO~2~/yr)
[@international_energy_agency_world_2021]. The application of CCS in industry
has been studied for different subsectors [@kuramochi_comparative_2012], but due
to the limited number of key processes, the large average size of installations
(which make for economic point sources) and the high specific CO~2~ emissions by
unit of economic output, the cement, chemicals, and steel subsectors are
considered to be main targets for CCS in industry [@naims_economics_2016-1].

```{r ces-tree, echo = FALSE, out.width = '12cm', out.height = '9.3cm', fig.cap = "REMIND CES production tree. Macroeconomic output (GDP) is produced from labour, energy (energy services), and capital, while energy services are subdivided into the industry sector and the buildings and transport sectors (both ommited for clarity). Industry subsectors cement, chemicals, steel, and other industry, use final energy (FE) provided by the energy system model, and energy efficiency capital (EEC) to provide energy services. $\\sigma$ denotes substitution elasticities between the respective inputs into a node, with $\\sigma = a \\sim b$ denoting elasticities changing over time."}
knitr::include_graphics('figures/CES_tree.png')
```

## Electrification of Heat Production

Energy use in industry can be coarsely subdivided into two categories: energy
for mechanical work, provided principally by electricity, and energy for
heating, provided principally by fuels -- especially on medium to high
temperature levels (above 100°C). Both are represented in REMIND through
individual nodes in the CES production structure of the industry subsectors,
with low elasticities between them, as mechanical work and heat are not
interchangeable in production processes. The substitution elasticities of fuels
increases from low to high levels (0.5 or 0.7 to 2) by 2040, reflecting lower
short-term and higher long-term flexibility in industry. Since the
electrification of heat production is technically possible [@madeddu_co2_2020-1]
and a viable option for mitigating CO~2~ emissions (when using carbon-free
electricity), we include an additional production factor for high-temperature
heat from electricity in the production functions for both chemicals and other
industry (cf. Figure \ref{fig:ces-tree}).

This allows for the explicit modelling of the substitution between fuels and
electricity in heat production, without allowing for the replacing of mechanical
work through heat. This option is only included for the chemicals and other
industry subsectors, since electric steel production is modelled explicitly (see
section \ref{Steel-Projections}) and electrification of clinker burning (a major
source of CO~2~ emissions in cement production) is less attractive compared to
CCS options, because it would only mitigate emissions from fuel burning, while
large process emissions from limestone calcination (about half of total
emissions) would remain (or require additional CCS).

The representation of energy carrier switching via the CES production function
takes into account the heterogeneity of circumstances in industry subsectors,
with some better positioned to electrify high-temperature heating than others.

## Energy-Efficiency Investments

We also introduce dedicated stocks for energy efficiency capital (EEC) for all
industry subsectors. They are positioned at the top level, such that subsector
output is produced from the aggregated energy inputs and the EEC (cf. Figure 
\ref{fig:ces-tree}). This models the trade-off between capital investment and
energy demand. It is possible to invest into facilities with higher energy
efficiency (usually for new installations, to some degree also for retrofits).
This capital stock is integrated into the handling of the macroeconomic capital
stock in REMIND, and is subject to depreciation and requires investments
[@baumstark_remind21_2021].

Both the stock of and the investments into capital for energy efficiency,
separate from the general capital for industrial production, are difficult to
ascertain [@iea_weio_2014]. We therefore initialise this stock from investment
estimates into energy-intensive (cement, chemicals, steel) and
non-energy-intensive industry (other industry) for 2014--20 [@iea_weio_2014,
Annex A], assuming a steady state where energy efficiency investments only cover
the depreciation of the EEC stock, which is assumed to depreciate exponentially
with a half-life of 25 years. For the baseline scenarios, we assume to EEC to
grow (and shrink) proportional to subsector output (but EEC stocks are not
reduced beyond the depreciation rate -- in accordance with the capital motion
formulation of the REMIND model).

## Representation of Mitigation Options

Expanding on @kaya_environment_1997 and @fischedick_industry_2014, industry
decarbonisation can be analysed using the identity

```{=tex}
\begin{equation}
\label{decomposition}
E = \mathrm{Pop} \times \frac{\mathrm{GDP}}{\mathrm{Pop}} \times \frac{A}{\mathrm{GDP}} 
    \times \frac{\mathrm{FE}}{A} \times \frac{\mathrm{FF}}{\mathrm{FE}} \times 
    \frac{C}{\mathrm{FF}} \times \frac{E}{C}
\end{equation}
```
With $E$ the emissions, $\mathrm{Pop}$ the population, $\mathrm{GDP}$ the
economic activity, $A$ the industrial production, $\mathrm{FE}$ the final energy
use in industry, $\mathrm{FF}$ the fossil fuels used in industry, and $C$ the
carbon content of those fossil fuels. The products of the identity are drivers
of emissions or mitigation options: $\mathrm{Pop}$ -- population growth (or
population change more general), $\mathrm{GDP}/\mathrm{Pop}$ -- per-capita GDP
(affluence), $A/\mathrm{GDP}$ -- the share of industry in economic activity (as
opposed to agriculture and services), $\mathrm{FE}/A$ -- the energy intensity of
industry, $\mathrm{FF}/\mathrm{FE}$ -- the share of fossil fuels in industry
energy demand (as opposed to renewable energies), $C/\mathrm{FF}$ -- the
specific carbon content of those fuels per unit energy (high for coal and oil,
lower for natural gas), and $E/C$ -- the emissions rate, which can be reduced by
CCS.

Different drivers and mitigation options are realised by different parts of the
extended REMIND model as presented here. Population is an exogenous SSP scenario
assumption and thus constant across scenarios (that are based on the same SSP).
GDP and industrial activity are endogenous elements of the production function
and vary with the strictness of mitigation constraints. Final energy, fossil
fuels, carbon content and emissions are all endogenous elements of the ESM.
Notably, the REMIND model covers the entire range of mitigation options, from
reduced demand for industrial goods, over increased energy efficiency in
industry, fuel-switching and renewable power production, to CCS.

A forthcoming overview of industry CO~2~ emission reduction
[@bauer_integrated_nodate] compares seven integrated assessment models "with
improved industry sector representation", in which the REMIND model as discussed
here shows the widest range of endogenous mitigation options. Notably, it is the
only IAM with endogenous reduction of industry demand, due to its integration of
a Ramsey growth model.

## Implementation of Mitigation Options in REMIND

The role of the industry module is to provide the hard link between
macroeconomic growth module and ESM, taking into account the aspects unique to
industry (as opposed to the buildings and transport sectors). This entails (1)
balancing industry final energy demand as an input to the CES production
function with final energy provision by the ESM, (2) deriving industry outputs
and activity levels consistent with the overall macroeconomic developments and
climate policy constraints, (3) deriving final energy demand consistent with
industrial outputs and activity levels, (4) accounting for CO~2~ emissions and
options for their abatement via CCS, and consistency of industrial production
with climate policy targets.

The first function is achieved by a simple balance equation that equates the
sums of final energy inputs of different types (solids, liquids, gases, ...)
into the different industry subsectors in the CES production function ($V_i$ in
equation \ref{production_function}) with the production of those final energy
carriers in the ESM. In that way, production of industry output that consumes
energy incurs costs for energy production in the ESM, which have to be covered
from the macroeconomic production (i.e. GDP output of the production function).

CES production functions, being an economic concept, do not represent all
physical aspects relevant to modelling physical production in industry. They
allow for the substitution of production factors beyond limits that might exist
in real technical applications. Specifically, the formulation with industry
subsector output being produced from energy and EEC captures the mechanism that
through investments into energy efficiency, the specific energy demand per unit
output can be reduced. But only up to a limit, given technical and physical (in
the limit thermodynamic) constraints. We therefore impose a lower bound of the
sum of final energy inputs into a subsectors' CES sub-tree per unit of subsector
output, to confine the model solution space to technically and physically
feasible values. This bound is described by an exponentially decreasing function
that decreases from the 2015 specific energy demand towards the limit described
below in section \ref{FE-Projections}, and passes through a point that allows
climate change mitigation scenarios to close no more than 75 % of the gap
between the baseline scenario and this limit in 2050.

CES production functions also do not deviate far from equilibrium points. This
is a problem in the detailed representation of final energy carriers with CES
functions, as those carriers not already in use will see little utilisation even
at very high price levels. This is most relevant for hydrogen and electricity
for high-temperature heat production in industry, both of which are economically
unattractive without strict climate policies and accompanying high carbon
prices, and are therefore not used so far. To overcome this limitation, we
employ two mechanisms.

First, we set future shares of hydrogen and high-temperature heat electricity
for the baseline calibration. The share of hydrogen in industry gases is
increased from 0.1 % in 2020 to 30 % in 2050. The share of high-temperature heat
electricity in industry FE demand for high-temperature heat (so excluding
electricity for mechanical work and low-temperature heat), is increased from 0.1
% in 2020 to 8 % in 2050. This leads to demand for hydrogen and high-temperature
heat electricity in industry that is not in line with a "no climate change
mitigation policy" baseline scenario, but necessary to generate realistic policy
scenarios.

Second, we apply mark-up costs to both hydrogen and high-temperature heat
electricity use in industry to represent additional cost of introducing new
technologies to the production process that use these energy carriers.

Due to the economic nature of input substitution in the production function, the
mark-up costs cannot be determined by techno-economic data and are instead set
based on model behaviour. Both mechanisms, future baseline
hydrogen/high-temperature heat electricity shares and mark-up costs, have been
parametrised utilising the concept of the marginal rate of substitution, which
describes the amount of one input needed to substitute another input to provide
the same economic value (the ratio of the partial derivatives of two inputs into
the production function). Final energy shares and mark-up cost are chosen such
that the marginal rates of substitution with respect to gases and liquids (as
the final energy carriers hydrogen and high-temperature heat electricity compete
most strongly with) roughly approach technical substitution ratios in climate
policy scenarios (one for hydrogen, two to three in for high-temperature
electricity). The mark-up costs can be reduced in scenarios which e.g. stipulate
a strong policy push for these technologies [@schreyer_role_nodate], making
hydrogen and/or high-temperature heat electricity cheaper relative to other
final energy carriers and in turn increasing their utilisation in industry.

Finally, we impose a lower bound on the share of steel from primary production
(cf. section \ref{Steel-Projections} below).

Industry CCS is calculated applying subsector-specific marginal abatement cost
(MAC) curves. The MAC curves are either based on [@kuramochi_comparative_2012],
a techno-economic assessment of CO~2~ capture technologies (with capture rates
of 28--76 % at costs of 62--133 \$US~2005~/t CO~2~[^3] for different
subsectors), or on [@fischedick_industry_2014, fig. 10.7--10.10], implementing a
more optimistic industry CCS scenario (with capture rates of 75 % at
50 \$US~2005~/t CO~2~ up to 95 % at 217 \$US~2005~/t CO~2~ for all subsectors
except "other industry"). Subsector emissions from fuel combustion are
calculated from final energy demand and fuel-specific emission factors. Process
emissions in the cement subsector are calculated by applying a specific
emissions factor for clinker (0.53 t CO~2~/t clinker) and regional clinker to
cement ratios (0.58-0.82 t clinker/t cement; @kermeli_enhancing_2016, fig. 21),
which are converged towards to lowest regional value by 2100. The model is then
able to capture industry CO~2~ emissions up to the level indicated by the
respective MAC curve at given CO~2~ prices. Since the MAC curves do not
reproduce the inertia of capital stocks required for CCS (investments and
retrofits would occur only gradually, not all at once), the increase in the
capture rate is limited to 5 % p.a.

[^1]: Most variables in REMIND vary with time. We omit time indices unless they
    are relevant for the equation at hand.

[^2]: Due to limited data availability, the "cement" subsector in the REMIND
    model encompasses the entire "non-metallic minerals" subsector, of which
    cement production is the dominant part.

[^3]: All monetary values and prices in REMIND are denoted in year 2005
    US-dollars and converted accordingly.
